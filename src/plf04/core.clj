(ns plf04.core)

(defn string-e-1
  [a]
  (<= 1 (letfn [(g [x]
                  (if (= (first x) \e)
                    1
                    0))
                (f [s]
                  (if (empty? s)
                    0
                    (+ (g s) (f (rest s)))))]
          (f a)) 3))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "HII")
(string-e-1 "e")
(string-e-1 "")

(defn string-e-2
  [a]
  (letfn [(f [s acc]
            (if (zero? (count s))
              (and (>= acc 1) (<= acc 3))
              (if (= (first s) \e)
                (f (rest s) (inc acc))
                (f (rest s) acc))))]
    (f a 0)))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "HII")
(string-e-2 "e")
(string-e-2 "")

(defn string-times-1
  [a n]
  (letfn [(f [s y]
            (if (zero? y)
              ""
              (str a (f s (dec y)))))]
    (f a n)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [a n]
  (letfn [(f [s y acc]
            (if (zero? y)
              acc
              (f s (dec y) (str s acc))))]
    (f a n "")))

(string-times-2 (list \H \i) 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

(defn front-times-1
  [a n]
  (letfn [(i [x y]
             (if (<= (count x) 3)
               (str x (f x (dec y)))
               (str (first x) (first (rest x)) (first (rest (rest x))) (f x (dec y)))))
          (f [s y]
            (if (or (zero? y) (empty? s))
              ""
              (i s y)
              ))]
    (f a n)))

(front-times-1 (list \C \h \o \c \o \l \a \t \e) 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [a n]
  (letfn [(i [x y z]
             (if (<= (count x) 3)
               (str x (f x (dec y) z))
               (str (first x) (first (rest x)) (first (rest (rest x))) (f x (dec y) z))))
          (f [s y acc]
            (if (or (zero? y) (empty? s))
              acc
              (i s y acc)
              ))]
    (f a n "")))


(front-times-2 "Chocolate" 2)
(front-times-2 (vector \C \h \o \c \o \l \a \t \e) 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)

(defn count-xx-1
  [a]
  (letfn [(g [x]
            (if (and (= \x (first x)) (= \x (first (rest x))))
              1
              0))
          (f [s]
            (if (empty? s)
              0
              (+ (g s)
                 (f (rest s)))))]
    (f a)))

(count-xx-1 "abcxx")
(count-xx-1 (list \x \x \x))
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

(defn count-xx-2
  [a]
  (letfn [(g [x y]
             (if (and (= \x (first x)) (= \x (first (rest x))))
               (f (rest x) (inc y))
               (f (rest x) y)))
          (f [s acc]
            (if (empty? s)
              acc
              (g s acc)
              ))]
    (f a 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")

(defn string-splosion-1
  [s]
  (letfn [(f [x]
            (if (== 0 (count x))
              x
              (apply str (f (subs x 0 (- (count x) 1))) x)))]
    (f s)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [a]
  (letfn [(f [s acc]
            (if (== 0 (count s))
              acc
              (f (subs s 0 (- (count s) 1)) (str s acc))))]
    (f a "")))

(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

(defn array-123-1
  [xs]
  (letfn [(g [ys]
             (if (and (= 1 (first ys))
                      (= 2 (first (rest ys)))
                      (= 3 (first (rest (rest ys)))))
               true
               (f (rest ys))))
          (f [ys]
            (if (or (empty? ys) (< (count ys) 3))
              false
              (g ys)))]
    (f xs)))


(array-123-1 [1 1 2 3 1])
(array-123-1 [1 2 3 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [xs]
  (letfn [(g [ys x]
            (if (and (= 1 (first ys))
                     (= 2 (first (rest ys)))
                     (= 3 (first (rest (rest ys)))))
              x
              (f (rest ys) x)))
          (f [ys acc]
            (if (or (empty? ys) (< (count ys) 3))
              (false? acc)
              (g ys acc)))]
    (f xs true)))



(array-123-2 [1 1 2 3 1])
(array-123-2 [1 2 3 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])

(defn string-x-1
  [s]
  (letfn [(g [x]
            (if (= (first x) \x)
              ""
              (str (first x))))
          (f [x]
            (if (or (empty? x) (= (count x) 1))
              ""
              (if (= (count (rest x)) 1)
                (str (first (rest x)))
                (str (g (rest x)) (f (rest x))))))]
    (str (first s) (f s))))

(string-x-1 (vector \x \x \H \x \i \x))
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
  [a]
  (letfn [(g [x y]
            (if (and (= \x (first x)) (> (count x) 1))
              (f (rest x) (inc y))
              (str (first x) (f (rest x) (inc y)))))
          (h [x y]
            (if (= y 0)
              (str (first x) (f (rest x) (inc y)))
              (g x y)))
          (f [s acc]
            (if (empty? s)
              ""
              (h s acc)))]
    (f a 0)))

(string-x-2 (vector \x \x \H \x \i \x))
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")

(defn alt-pairs-1
  [a]
  (letfn [(j [x]
             (if (< 1 (count x))
               (str (first x) (first (rest x)))
               (str first x)))
          (i [x]
             (if (< 3 (count x))
               (str (first x) (first (rest x)) (f (subs x 4)))
               (j x)
               ))
          (h [x]
             (if (< 2 (count x))
               (str (subs x 0 2) (f (subs x 4)))
               (subs x 0 2)))
          (g [x]
             (if (even? (count x))
               (h x)
               (i x)
               ))
          (f [s]
            (if (empty? s)
              ""
              (g s)
              ))]
    (f a)))

(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

(defn alt-pairs-2
  [a]
  (letfn [(j [x]
             (if (< 1 (count x))
               (str (first x) (first (rest x)))
               (str first x)))
          (i [x y]
             (if (< 3 (count x))
               (str (first x) (first (rest x)) (f (subs x 4) y))
               (j x)))
          (h [x y]
             (if (< 2 (count x))
               (str (first x) (first (rest x)) (f (subs x 4) y))
               (str (first x) (first (rest x)))))
          (g [x y]
             (if (even? (count x))
               (h x y)
               (i x y)
               ))
          (f [s acc]
            (if (and (empty? s))
              acc
              (g s acc)
              ))]
    (f a "")))

(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

(defn string-yak-1
  [s]
  (letfn [(g [x y]
            (and (= \y (first x)) (= \k (first (rest y)))))
          (h [x]
             (if (g x (rest x))
               (f (rest (rest (rest x))))
               (str (first x) (f (rest x)))))
          (f [x]
            (if (empty? x)
              ""
              (h x)))]
    (f s)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [a]
  (letfn [(g [x y]
             (and (= \y (first x)) (= \k (first (rest y)))))
          (h [x y]
             (if (g x (rest x))
               (f (rest (rest (rest x))) y)
               (f (rest x) (str y (first x)))))
          (f [s acc]
            (if (empty? s)
              acc
              (h s acc)))]
    (f a "")))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")

(defn has-271-1
  [xs]
  (letfn [(g [x]
            (and (= (+ 5 (first x)) (first (rest x)))
                 (>= 2 (h x))))
          (h [x]
            (if (neg? (- (first (rest (rest x))) (- (first x) 1)))
              (* -1 (- (first (rest (rest x))) (- (first x) 1)))
              (- (first (rest (rest x))) (- (first x) 1))))
          (f [x] (if (empty? x)
                   false
                   (if (g x)
                     true
                     (f (rest x)))))]
    (f xs)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [xs]
  (letfn [(g [x]
            (and (= (+ 5 (first x)) (first (rest x)))
                 (>= 2 (h x))))
          (h [x]
            (if (neg? (- (first (rest (rest x))) (- (first x) 1)))
              (* -1 (- (first (rest (rest x))) (- (first x) 1)))
              (- (first (rest (rest x))) (- (first x) 1))))
          (f [x acc] (if (empty? x)
                   (false? acc)
                   (if (g x)
                     acc
                     (f (rest x) acc))))]
    (f xs true)))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])